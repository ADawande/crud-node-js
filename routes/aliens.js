const express = require('express')
const router = express.Router()
const Alien = require('../model/alien')


router.get('/', async(req, res) => {
    try {
        const aliens = await Alien.find()
        res.json(aliens)
    } catch (err) {
        res.send('Error ' + err)
    }
})

router.get('/:id', async(req, res) => {
    try {
        const alien = await Alien.findById(req.params.id)
        res.json(alien)
    } catch (err) {
        res.send('Error ' + err)
    }
})


router.post('/', async(req, res) => {
    const alien = new Alien({
        name: req.body.name,
        tech: req.body.tech,
        position: req.body.position
    })

    try {
        const a1 = await alien.save()
        res.json(a1)
    } catch (err) {
        res.send('Error')
    }
})

router.patch('/:id', async(req, res) => {
    try {
        const alien = await Alien.findById(req.params.id)
        alien.tech = req.body.tech
        const a1 = await alien.save()
        res.json(a1)
    } catch (err) {
        res.send('Error')
    }
})

router.delete('/:id', async(req, res) => {
    try {
        const alien = await Alien.findById(req.params.id)
        const a2 = await alien.delete()
        res.json(a2)
    } catch (error) {
        res.send('Error')
    }
})

module.exports = router